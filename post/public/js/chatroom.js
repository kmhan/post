(function connect(){
    let socket = io.connect('http://localhost:3000')
    	
	let message = document.querySelector('#msg')
	let messageBtn = document.querySelector('#msgBtn')
	let messageList = document.querySelector('#msg-list')

	messageBtn.addEventListener('click', e => {
		console.log(message.value)
		socket.emit('new_message', {message: message.value})
		message.value = ''
	})

	socket.on('receive_message', data => {
		console.log(data)
		let listItem = document.createElement('li')
		listItem.textContent = data.message
		listItem.classList.add('list-group-item')
		messageList.appendChild(listItem)
	})
})()